package com.tw.restdemo.vo;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Component
public class GreetingContent {

    @NotNull(message = "FAILURE : sender can't be null.")
    @NotEmpty(message = "FAILURE : sender can't be empty.")
    private String sender;

    @NotNull(message = "FAILURE : Receiver can't be null.")
    @NotEmpty(message = "FAILURE : Receiver can't be empty.")
    private String receiver;

    @NotNull(message = "FAILURE : title can't be null.")
    @NotEmpty(message = "FAILURE : title can't be empty.")
    private String title;

    @NotNull(message = "FAILURE : subtitle can't be null.")
    @NotEmpty(message = "FAILURE : subtitle can't be empty.")
    private String subtitle;

    public String getReceiver() {
        return receiver;
    }

    public String getSender() {
        return sender;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getTitle() {
        return title;
    }
}
