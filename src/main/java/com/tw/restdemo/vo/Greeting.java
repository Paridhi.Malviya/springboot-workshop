package com.tw.restdemo.vo;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

@Component
public class Greeting {

    private String content;

    @Autowired
    GreetingContent greetingContent;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setCustomMessage(String message) {
        content = "welcome to POST request " + message;
    }

    public void setGreetingContent(GreetingContent greetingContent) {
        this.greetingContent = greetingContent;
    }

    public GreetingContent getGreetingContent() {
        return greetingContent;
    }
}
