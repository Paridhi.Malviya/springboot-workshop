package com.tw.restdemo.controller;

import com.tw.restdemo.vo.Greeting;
import com.tw.restdemo.vo.GreetingContent;
import com.tw.restdemo.vo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
public class WelcomeController {

    @Autowired
    Greeting greeting;

    @GetMapping("/hello")
    public String hello() {
        return "Hi, Welcome to Springboot.";
    }

    //request parameter in URL
    @GetMapping("/helloName")
    public String hello(@RequestParam(value = "name", defaultValue = "UNKNOWN") String name) { //http://localhost:8081/helloName?name=
        return "Hello " + name;
    }

    //With request body
    @GetMapping("/helloUser")
    public String helloUser(@RequestBody User user) {
        return "Hello to request body of Springboot " + user.getName();
    }

    @PostMapping("/helloUserGreeting") //http://localhost:8081/helloUserGreeting
    public @ResponseBody
    Greeting helloGreeting(@RequestBody  User user) {

        Greeting greeting = new Greeting();
        greeting.setContent("For User Object,we are returning Greeting object. Hello "+ user.getName());
        return greeting;
    }

    @PostMapping("/greeting")
    public String createGreeting(@RequestBody String message) {
        greeting.setCustomMessage(message);
        return "SUCCESS!";
    }

    @GetMapping("/greeting")
    public String getGreeting() {
        return greeting.getContent();
    }

    @PostMapping("/greetingContent")
    public String greetingContent(@RequestBody @Valid GreetingContent greetingContent, BindingResult results) {

        if (results.hasErrors()) {
            return String.valueOf(results.getAllErrors());
        }
        greeting.setGreetingContent(greetingContent);
        return "SUCCESS";
    }

    @GetMapping("/greetingContent")
    public @ResponseBody
    GreetingContent greetingContent() {
        return greeting.getGreetingContent();
    }
}
